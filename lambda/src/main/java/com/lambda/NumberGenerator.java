package com.lambda;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NumberGenerator {
    private Random generator = new Random();

    private int GetRandom()
    {
        return 10 + generator.nextInt(90);
    }

    public List<Integer> GetListOfRandomNumbers()
    {
        return Stream.iterate(GetRandom(), n ->GetRandom()).limit(10).collect(Collectors.toList());
    }
}
