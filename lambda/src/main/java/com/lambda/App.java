package com.lambda;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;

public class App
{
    private static void print(Object o)
    {
        System.out.print(o);
    }

    private static void println(Object o)
    {
        System.out.println(o);
    }

    private static void println()
    {
        System.out.println();
    }

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        LambdaFunctions Lambda = new LambdaFunctions();
        int result = Lambda.getAverage.Lambda(1,2,3);
        result = Lambda.getMax.Lambda(1,2,3);

        ILambda op = new ILambda(){

            public int Lambda(int a, int b, int c)
            {
                return a+b+c;
            }
        };
        int operation1 = op.Lambda(20, 10, 30);
        ILambda operation2 = (x, y, z)-> (x + y +z ) / 3;

        NumberGenerator numberGenerator = new NumberGenerator();
        List list = numberGenerator.GetListOfRandomNumbers();

        Supplier<IntStream> streamSupplier
                = () -> list.stream().mapToInt(value -> (int)value);

        streamSupplier.get().forEach((n -> print(n + "\t")));

        println();
        println();

        double average = streamSupplier.get().average().getAsDouble();
        println("Size = " + streamSupplier.get().count());
        println("Average = " + average);
        println("Min = " + streamSupplier.get().reduce(Math::min).getAsInt());
        println("Max = " + streamSupplier.get().reduce(Math::max).getAsInt());
        println("Sum = " + streamSupplier.get().sum());
        println("Bigger than average = " + streamSupplier.get().filter(n-> (double)n > average).count());
    }
}
