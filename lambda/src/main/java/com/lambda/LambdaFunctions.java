package com.lambda;

import java.util.stream.IntStream;

public class LambdaFunctions {
    public ILambda getMax = (int a, int b, int c) -> IntStream.of(a,b,c).max().getAsInt();
    public ILambda getAverage = (int a, int b, int c) -> (int) Math.round(IntStream.of(a,b,c).average().getAsDouble());
}
